//mention the line you modified
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        //change made below
        if (num == 0 && num == 1) {
            System.out.println("The given number is neither prime nor composite");
        } else {
            if (isPrime(num)) {
                System.out.println("The given number is a prime number");
            } else {
                System.out.println("The given number is not a prime number");
            }
        }
    }

    public static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
